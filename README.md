# OpenML dataset: Fake-News

https://www.openml.org/d/43592

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Fake news has become one of the biggest problems of our age. It has serious impact on our online as well as offline discourse. One can even go as far as saying that, to date, fake news poses a clear and present danger to western democracy and stability of the society.
Content
What's inside is more than just rows and columns. Make it easy for others to get started by describing how you acquired the data and what time period it represents, too.
Acknowledgements
We wouldn't be here without the help of others. If you owe any attributions or thanks, include them here along with any citations of past research.
Inspiration
Your data will be in front of the world's largest data science community. What questions do you want to see answered?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43592) of an [OpenML dataset](https://www.openml.org/d/43592). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43592/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43592/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43592/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

